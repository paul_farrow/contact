<?php

namespace Paulfarrow\Contact;
use App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Paulfarrow\Contact\Requests\ContactFormRequest;


/**
 * Class ContactController
 * @package Paulfarrow\Contact
 */
class ContactController extends Controller
{
    /**
     * Displays the form content according to the configured default or selected template.
     *
     * @param int $template
     * @return mixed
     */
    public function showForm($template = 0){
        // uses the template set in the config file if a specific template is not defined as part of the request
        $template = ($template===0)?config('contact.form_template'):'contact'.$template;

        return view('contact::'.$template);
    }

    /**
     * Processes the submission of the contact us form.
     *
     * @param ContactFormRequest $request
     * @return mixed
     */
    public function processSubmission(ContactFormRequest $request){
        // store contact request in the database
        $contactRequest = ContactRequest::create($request->input());

        // send email with contact request details
        $this->sendContactRequestEmail($contactRequest);

        // redirect to the confirmation page
        return redirect('contact_confirmation');
    }

    /**
     * Send an email to the configured email with details of the request.
     *
     * @param ContactRequest $contactRequest
     */
    private function sendContactRequestEmail(ContactRequest $contactRequest){
        Mail::send('contact::contactemail', $contactRequest->toArray(), function ($message) {

            // use package configuration for name and address
            // otherwise use default mail package 'from' address and 'name' values
            $company_name = (config('contact.company_name')=="")?
                                config('mail.from.name'):config('contact.company_name');
            $email = (config('contact.email_address')=="")?
                                config('mail.from.address'):config('contact.email_address');

            // set 'from' address for email
            $message->from($email, $company_name);

            // set the destination email address
            $message->to($email)->subject('Contact form query');
        });
    }

    /**
     * Displays the submission confirmation page.
     *
     * @return mixed
     */
    public function confirmation(){
        return view('contact::confirmation');
    }


    /**
     * Shows the contact requests that have been submitted
     *
     * @return mixed
     */
    public function showRequests(){
        // Authorisation would need to be added here when integrated into an application.
        // This would determine if the current user was authorised to view the contact requests.

        // Retrieve all the contact requests
        $contactRequests = ContactRequest::paginate(config('contact.pagination_value'));

        // checks if page number needs to be fixed
        $contactRequests = $this->paginationFix($contactRequests);

        return view('contact::contactRequests')->with('contactRequests',$contactRequests);
    }


    /**
     * Fixes issue where if the user deletes the last entry on page, it shows an empty page
     * rather than last pagination page.
     *
     * @param $contactRequests
     */
    private function paginationFix($contactRequests){
        //  checks if current page is beyond the last page of results
        if ($contactRequests->lastPage()<$contactRequests->currentPage()) {

            // gets the last available page
            $lastPage = $contactRequests->lastPage();

            // sets the pagination class to use the last page
            Paginator::currentPageResolver(function() use ($lastPage) {
                return $lastPage;
            });

            // retrieves new results for the new page number
            $contactRequests = ContactRequest::paginate(config('contact.pagination_value'));
        }

        return $contactRequests;
    }

    /**
     * Soft deletes the given contact request
     *
     * @param ContactRequest $contactRequest
     * @return mixed
     */
    public function destroy(ContactRequest $contactRequest)
    {
        // Authorisation would need to be added here when integrated into an application.
        // This would determine if the current user was authorised to delete the selected contact requests.

        // soft delete the contact request
        $contactRequest->delete();

        return redirect()->back();
    }

}