@extends(config('contact.layout'))

@section('content')

<h1>Thank you for contacting us</h1>
<p>Your request has been sent to one of our team and we will get back to you shortly.</p>

@endsection