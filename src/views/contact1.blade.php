@extends(config('contact.layout'))

@section('content')

<h1>Contact Us</h1>
<form id="contact" method="post" action="{{ url('contact') }}" class="form" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    @include('contact::common.errors')

    <div class="row">
        <div class="col-xs-6 col-md-6 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" value="{{ old('name') }}" autofocus required>
        </div>
        <div class="col-xs-6 col-md-6 form-group">
            <input class="form-control" type="email" id="email" name="email" placeholder="Email" value="{{ old('email') }}" required>
        </div>
    </div>
    <textarea class="form-control" id="msg" name="msg" placeholder="Message" rows="5" required>{{ old('msg') }}</textarea>
    <br>
    <div class="row">
        <div class="col-xs-12 col-md-12 form-group">
            <button class="btn btn-primary pull-right" type="submit">Submit</button>
        </div>
    </div>
</form>

@endsection