<p>There has been a contact request posted from the website contact form, the details are shown below.</p>
<br>
<strong>Name:</strong> {{ $name }} <br>
<strong>Email address:</strong> {{ $email }} <br>
<strong>Message:</strong><br>
{{ $msg }}