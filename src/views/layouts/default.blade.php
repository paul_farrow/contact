<!DOCTYPE html>
<html lang="en">
<head>
    <title>Basic Contact Form Package</title>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Bootstrap Resources -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {
            // set the correct header as active in the menu based on url
            var pathname = window.location.pathname;
            $('.nav > li > a[href$="'+pathname+'"]').parent().addClass('active');
        })
    </script>


</head>

<body>

<!-- NavBar Start -->
<div class="container-fluid">
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('contact') }}">Dev Layout</a>
        </div>
        <div class="collapse navbar-collapse" id="mainNavbar">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('contact') }}">Default Form</a></li>
                <li><a href="{{ url('contact/1') }}">Template1</a></li>
                <li><a href="{{ url('contact/2') }}">Template2</a></li>
                <li><a href="{{ url('contact_confirmation') }}">Confirmation Page</a></li>
                <li><a href="{{ url('contact_requests') }}">Contact Requests</a></li>
            </ul>
        </div>
    </nav>
</div>
<!-- NavBar End -->

<!-- Main Content -->
<div class="container">
    @yield('content')
</div>

</body>
</html>