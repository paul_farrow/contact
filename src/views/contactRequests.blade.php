@extends(config('contact.layout'))

@section('content')

@if (count($contactRequests) > 0)
<div class="panel panel-default">
    <div class="panel-heading">
        Current Contact Requests
    </div>

    <div class="panel-body">
        <p class="bg-info">Please click on a request to see more details.</p>
        <table class="table table-striped table-condensed">

            <thead>
                <tr>
                    <th>Contact Name</th>
                    <th>Received On</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
            @foreach ($contactRequests as $contactRequest)
            <tr>
                <!-- Contact Request Name -->
                <td data-toggle="collapse" data-target="#request{{ $contactRequest->id }}" class="table-text accordion-toggle">
                    <div><strong>{{ $contactRequest->name }}</strong></div>
                </td>

                <td data-toggle="collapse" data-target="#request{{ $contactRequest->id }}" class="table-text accordion-toggle">
                    <div>{{ $contactRequest->created_at }}</div>
                </td>

                <td>
                    <form action="{{ url('contact/'.$contactRequest->id) }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}

                        <button type="submit" id="delete-contactRequest-{{ $contactRequest->id }}" class="btn btn-xs btn-danger">
                            Delete
                        </button>
                    </form>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="hiddenRow">
                    <div class="accordian-body collapse" id="request{{ $contactRequest->id }}" >
                        <div><strong>Email:</strong> {{ $contactRequest->email }}</div>
                        <div><strong>Message:</strong> {{ $contactRequest->msg }}</div>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        {!! $contactRequests->links() !!}
    </div>
</div>
@endif

@endsection