<?php
/**
 * Basic configuration options for the contact form
 */

return [

    'company_name' => 'TDI',
    'email_address' => 'youremail@example.com',
    'layout' => 'contact::layouts.default',
    // choose between 'contact1' and 'contact2' form templates
    'form_template' => 'contact1',
    'pagination_value' => 10,
];