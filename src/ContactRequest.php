<?php

namespace Paulfarrow\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactRequest
 * @package Paulfarrow\Contact
 */
class ContactRequest extends Model
{
    // use soft deletes to keep records of all contact requests
    use SoftDeletes;
    
    // Add deleted_at field to dates property
    protected $dates = ['deleted_at'];
    
    // allows mass assignment main contact fields
    protected $fillable = ['name','email','msg'];

    // configures what fields to export in array and json conversions
    protected $visible = array('name','email','msg');

}
