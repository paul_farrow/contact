<?php

namespace Paulfarrow\Contact\Requests;

use App\Http\Requests\Request;

/**
 * Class ContactFormRequest
 * @package Paulfarrow\Contact\Requests
 */
class ContactFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // can add authorisation logic here if required
        return true;
    }

    /**
     * Get the validation rules that apply to the contact form request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'msg' => 'required'
        ];
    }

    /**
     * Specify custom error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'msg.required' => 'The message field is required.',
        ];
    }
}
