<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('contact/{template?}', ['as' => 'contact', 'uses' => 'paulfarrow\contact\ContactController@showForm']);
    Route::get('contact_confirmation', ['as' => 'contact_confirmation', 'uses' => 'paulfarrow\contact\ContactController@confirmation']);
    Route::get('contact_requests', ['as' => 'contact_requests', 'uses' => 'paulfarrow\contact\ContactController@showRequests']);
    Route::post('contact', ['as' => 'contact', 'uses' => 'paulfarrow\contact\ContactController@processSubmission']);
    Route::delete('contact/{contactRequest}', ['as' => 'contact', 'uses' => 'paulfarrow\contact\ContactController@destroy']);
});

