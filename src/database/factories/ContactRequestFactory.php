<?php


$factory->define(Paulfarrow\Contact\ContactRequest::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'msg' => $faker->paragraph(rand(1,5)),
    ];
});