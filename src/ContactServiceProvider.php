<?php

namespace Paulfarrow\Contact;

use Illuminate\Support\ServiceProvider;

/**
 * Class ContactServiceProvider
 * @package Paulfarrow\Contact
 * 
 * Handles the publishing and registering of package resources.
 */
class ContactServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'contact');
        $this->publishes([
            __DIR__.'/views' => resource_path('views/paulfarrow/contact'),
            __DIR__.'/../tests' => base_path('tests/contact'),
            __DIR__.'/config/contact.php' => config_path('contact.php'),
            __DIR__.'/database/migrations' => database_path('migrations'),
            __DIR__.'/database/factories' => database_path('factories'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // merge configuration with any existing configuration options
        $this->mergeConfigFrom(
            __DIR__.'/config/contact.php', 'contact'
        );

        // configure routes for the contact form
        require __DIR__.'/routes.php';

        $this->app->make('Paulfarrow\Contact\ContactController');
    }
}
