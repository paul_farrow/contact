<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;


/**
 * Class ContactControllerTest
 * Runs tests required for the contact us form to operate successfully.
 */
class ContactControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A simple test to confirm form submission is working and redirecting to confirmation page.
     *
     * @return void
     */
    public function testContactUsForm()
    {
        $this->visit('/contact')
            ->type('John Smith', 'name')
            ->type('user@example.com','email')
            ->type('Test query.','msg')
            ->press('Submit')
            ->seePageIs('/contact_confirmation');
    }

    /**
     * A simple test to confirm required form fields are being checked correctly.
     *
     * @return void
     */
    public function testRequiredFieldValidation()
    {
        $this->visit('/contact')
            ->type('John Smith', 'name')
            ->type('Test query.','msg')
            ->press('Submit')
            ->seePageIs('/contact')
            ->see('field is required');

        $this->visit('/contact')
            ->type('user@example.com','email')
            ->type('Test query.','msg')
            ->press('Submit')
            ->seePageIs('/contact')
            ->see('field is required');

        $this->visit('/contact')
            ->type('John Smith', 'name')
            ->type('user@example.com','email')
            ->press('Submit')
            ->seePageIs('/contact')
            ->see('field is required');
    }

    /**
     * A simple test to confirm email validation is being checked correctly.
     *
     * @return void
     */
    public function testEmailAddressValidation()
    {
        $this->visit('/contact')
            ->type('John Smith', 'name')
            ->type('example.com','email')
            ->type('Test query.','msg')
            ->press('Submit')
            ->seePageIs('/contact')
            ->see('The email must be a valid email address.');
    }

    /**
     * Checks the form data is being stored in the database
     *
     * @return void
     */
    public function testDatabaseInsertion(){
        $this->visit('/contact')
            ->type('John Smith', 'name')
            ->type('user@example.com','email')
            ->type('Test query.','msg')
            ->press('Submit');

        $this->seeInDatabase('contact_requests', ['name' => 'John Smith', 'email' => 'user@example.com', 'msg' => 'Test query.']);
    }


    /**
     * Tests if the contact_requests page displays newly created contact requests
     *
     * @return void
     */
    public function testViewRequestsPage(){
        // generate some sample data using the ContactRequest Factory
        $contactRequests = factory(Paulfarrow\Contact\ContactRequest::class,5)->create();

        // test to see if last created contact request is visible on the last page
        $this->visit('/contact_requests?page=9999')
            ->see('Current Contact Requests')
            ->see($contactRequests[4]->name);
    }

    /**
     * Tests to see if the pagination system is working correctly
     *
     * @return void
     */
    public function testPagination(){
        // get the current pagination configuration
        $itemsOnPage = config('contact.pagination_value');

        // generate some sample data using the ContactRequest Factory
        $contactRequests = factory(Paulfarrow\Contact\ContactRequest::class,$itemsOnPage+1)->create();

        // test to see if first and last contact requests generated are on separate pages
        $this->visit('/contact_requests?page=9999')
            ->see($contactRequests[$itemsOnPage]->name)
            ->dontSee($contactRequests[0]->name);
    }

    /**
     * Tests the delete button functionality on the contact_requests page
     *
     * @return void
     */
    public function testDeleteButton(){
        // generates new random contact request
        $contactRequest = factory(Paulfarrow\Contact\ContactRequest::class)->create();

        // first checks the new requests is visible on the last page of the contact_requests page
        $this->visit('/contact_requests?page=9999')
            ->see($contactRequest->name)
            // presses the associated delete button that conrresponds to the new contact request
            ->press('delete-contactRequest-'.$contactRequest->id)
            // checks the new contact requests has been removed and is no longer visible
            ->seePageIs('/contact_requests?page=9999')
            ->dontSee($contactRequest->name);
    }

}
